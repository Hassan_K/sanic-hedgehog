import React from 'react';
import "./text.css"
export const Text = () => {
  return (
    <div className='text-center ml-[10px] md:ml-0'>
      <h1 className="txt text-[24px] font-bold " style={{ color: '#764824', fontFamily: "Bitter, sans-serif" }}>Sanic the cutest hedgehog on solana</h1>
    </div>
  );
};
