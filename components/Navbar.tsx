import React from "react";
import { useState } from "react";
import { Link } from "react-scroll/modules";
import { usePathname } from "next/navigation";
import Image from "next/image";

interface NavItem {
  label: string;
  page: string;
}

const NAV_ITEMS: Array<NavItem> = [
  {
    label: "SANIC",
    page: "home",
  },
];

export default function Navbar() {
  const pathname = usePathname();
  const [navbar, setNavbar] = useState(false);

  return (
    <header
      className="w-full mx-auto px-4 sm:px-20 top-0 z-0 text-[#764824]"
      style={{
        backgroundColor: "#FBECCF",
        fontFamily: "'Bitter', sans-serif",
        fontSize: "29px",
        color: "#764824",
        fontWeight: "bold",
      }}
    >
      <div className="flex justify-between items-center">
        <div>
          <div className="flex items-center py-3 md:py-5 md:block">
            <Image
              src="/hedgehog-removebg-preview.png"
              alt=""
              width={80}
              height={80}
              className="border-solid border-2 border-[#764824] rounded-full"
            />
          </div>
        </div>

        {/* Mobile View: Show links on the extreme right */}
        <div className="md:hidden flex justify-end">
          <div className="items-center space-x-6">
            {NAV_ITEMS.map((item, idx) => (
              <Link
                key={idx}
                to={item.page}
                className="text-[#764824] cursor-pointer"
                activeClass="active"
                spy={true}
                smooth={true}
                offset={-100}
                duration={500}
                onClick={() => setNavbar(!navbar)}
                style={{
                  fontFamily: "'Bitter', sans-serif",
                  fontSize: "29px",
                  fontWeight: "bold",
                  letterSpacing: "1.6px",
                }}
              >
                {item.label}
              </Link>
            ))}
          </div>
        </div>

        {/* Desktop View: Show links in the middle */}
        <div className="hidden md:block">
          <div className="flex space-x-6">
            {NAV_ITEMS.map((item, idx) => (
              <Link
                key={idx}
                to={item.page}
                className="text-[#764824] cursor-pointer"
                activeClass="active"
                spy={true}
                smooth={true}
                offset={-100}
                duration={500}
                onClick={() => setNavbar(false)}
                style={{
                  fontFamily: "'Bitter', sans-serif",
                  fontSize: "29px",
                  fontWeight: "bold",
                  letterSpacing: "1.6px",
                }}
              >
                {item.label}
              </Link>
            ))}
          </div>
        </div>
      </div>
    </header>
  );
}
